---
title: Zendesk Explore Important Dashboards
description: Support Operations documentation page for Zendesk Explore's Important Dashboards
canonical_path: "/handbook/support/readiness/operations/docs/zendesk/zendesk-explore/important_dashboards"
---

## Important Dashboards

Support Readiness has created few dashboards in Explore and Tableau to make data easy to understand. Following
is the list of some important dashboards with their description:

### Explore Dashboards

#### Organization Performance

This Dashboard is often used to find how support team is performing specific to an Organization (Customer). This dashboard
includes metrics like SLA Performance, Longest Solved Tickets, Tickets Category, FRT/NRT or Priority of tickets
etc. All Light agents who have access to Zendesk can view the [Organization Performance Dashboard](https://gitlab.zendesk.com/explore/studio#/dashboards/47EB07BFD95FB76D1FB0F697673A6A6348E2804DC81A2A0608B09458AF9E4ABC)

#### Support KPIs Dashboard

This dashboard is used to monitor & track Support KPIS, both Handbook (SSAT, FRT, NRT, Customer Wait Time, FRT Breaches) and Non-Handbook (MHT, MTTR)
It provides a look into KPI performance for the last 4 weeks and shows 12-month trends of key instances.
The Support KPIs Dashboard can be accessed [here](https://gitlab.zendesk.com/explore/studio#/dashboards/3DC60497A02C9E0EDB02ECE9C20153733D4AF220B656C550418FF2E42B7E2329)
